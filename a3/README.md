
# LIS3781 Advanced Database Management

## Chris Ehster

### Assignment 3 Requirements:
1. Log in to an Oracle VM and use SQL to determine version and users.
2. Create and populate tables in the Oracle DBMS.

#### README.md file should include the following items:

* A brief description of the assignment.
* Several git commands used in the creation of this assignment, as well as short descriptions of each command listed.
* Images of SQL and relevant data.

#### Git commands w/short descriptions:

1. git init - This command creates a git repository.
2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).

#### Assignment Screenshots:

*Screenshot of SQL code*:

![SQL Screenshot](img/sql.png)

*Screenshot of populated tables*:

![Populated tables Screenshot](img/tables.png)


