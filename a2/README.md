
# LIS3781 Advanced Database Management

## Chris Ehster

### Assignment 2 Requirements:
1. Setup a Bitbucket account and learn about using git version control in the Bitbucket environment.
2. Create a local database using AMPPS and use the SQL provided below to populate the database.
3. Manage users and permissions on the created server.

#### README.md file should include the following links:

* Main LIS3781 repository - https://bitbucket.org/cehster/lis3781/src
* BitbucketStationLocations demo repository - https://bitbucket.org/cehster/bitbucketstationlocations/src

#### Git commands w/short descriptions:

1. git init - This command creates a git repository.
2. git status - This command checks the local repo for any changes. It compares the local repo to a snapshot of the remote repo that was pulled/cloned, and displays stages of said changes (staged, committed).
3. git add - This command 'stages' changes you made to a repository, which adds it to a queue to prepare for committing.
4. git commit - This command properly commits the changes you've made, but only on the local repository you are working with. To your local machine, you have officially saved your changes.
5. git push - This command sends the changes you've committed to the remote repository associated with the directory. This officially saves changes for the remote repo.
6. git pull - This command grabs the working master copy of the remote repository selected and brings in any changes to your local working repo that may have been added since the last pull.
7. git branch - This command takes an existing repo and creates a copy that can be used for feature development (so as not to disturb the master branch for production ready projects).

#### Assignment Screenshots:

*Screenshot of SQL code*:

![SQL Screenshot](img/code1.png)
![SQL Screenshot](img/code2.png)
![SQL Screenshot](img/code3.png)

*Screenshot of populated tables*:

![Populated tables Screenshot](img/populatedTables.png)


