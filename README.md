# LIS3781 - Advanced Database Management

## Chris Ehster

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Create an Entity Relationship Diagram from a set of given business rules
    - Create and populate tables with SQL

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a Bitbucket repository
    - Install AMPPS and setup a local mySQL server
    - Create several test users and populate a small database

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a database in the Oracle DBMS
    - Create tables and insert values with SQL in Oracle
    - Check Oracle configuration

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Design and model a larger database that includes time-variant tables and a specialization hierarchy
    - Hash and `salt` sensitive data with the SHA2 hashing algorithm and a stored procedure
    - Manage time-variant data with triggers and events

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Design and model a database with a specialization hierarchy for MSSQL
    - Design a hashing procedure in T-SQL
    - Create an ERD using the MSSQL software tools
    - Populate data using other tables

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Using the data model from the previous assignment, design a data mart as a buisiness intelligence platform for data warehousing
    - Model the data mart with MSSQL ERD tools
    - Create a snowflake data schema